let temp = true;
function change() {
    if (temp) {
        document.getElementById("basic").innerHTML = "&dollar;19.99";
        document.getElementById("Professional").innerHTML = "&dollar;24.99";
        document.getElementById("Master").innerHTML = "&dollar;39.99";
        temp = false;
    } else {
        document.getElementById("basic").innerHTML = "&dollar;199.99";
        document.getElementById("Professional").innerHTML = "&dollar;249.99";
        document.getElementById("Master").innerHTML = "&dollar;399.99";
        temp = true;
    }
}

let check = document.querySelector("input[type=checkbox]");
check.addEventListener("click", change);
